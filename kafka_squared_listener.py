from kafka import KafkaConsumer

server = "172.31.41.72:9092"


consumer = KafkaConsumer("squared", bootstrap_servers=[server])

print("listening")
for message in consumer:
    simple, squared = message.value.decode().split("->")
    print(f"squared {simple} to {squared}")
