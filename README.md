# kafka-python-example-for-iot

## Принцип работы

- kafka_producer генерирует числа и пишет в топик to_square
- kafka_squarer возводит в квадрат числа из топика to_square и пишет их в топик squared в формате `{число}->{число в квадрате}`
- kafka_squared_listener читает из топика squared и выводит результат 