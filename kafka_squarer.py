from kafka import KafkaConsumer
from kafka import KafkaProducer

server = "172.31.41.72:9092"


consumer = KafkaConsumer("to_square", bootstrap_servers=[server])
producer = KafkaProducer(bootstrap_servers=[server])

print("listening")
for message in consumer:
    print("to_square:", int(message.value.decode()))
    producer.send(
        "squared",
        f"{message.value.decode('utf-8')}->{int(message.value.decode('utf-8')) ** 2}".encode(),
    )
