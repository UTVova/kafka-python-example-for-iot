import random
from time import sleep

from kafka import KafkaProducer

server = "172.31.41.72:9092"


producer = KafkaProducer(bootstrap_servers=[server])

print("generating")
while True:
    value = str(random.randint(1, 1000))
    producer.send("to_square", value.encode())
    print("generated:", value)
    sleep(0.3)
